# Assignment 7

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a07`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*100%*)

Suppose you are given a list of students' names and a list of their scores:
```python
['Mary', 'Jack', 'Rose', 'Mary', 'Carl', 'Fred', 'Meg', 'Phil', 'Carl', 'Jack', 'Fred', 'Mary', 'Phil', 'Jack', 'Mary', 'Fred', 'Meg']
```
and a list of their scores for different assignments:
```python
[80, 88, 53, 80, 64, 61, 75, 80, 91, 82, 68, 76, 95, 58, 89, 51, 81, 78]
```

Write a function `average_grades(names, grades)` which takes such lists as parameters and returns a *dictionary* consisting of a name and the *average* grade for each student. In the example lists given above, you would have
```python
{'Mary': 76.4, 'Jack': 70.0, ...}
```
The function should be able to work with generic lists, in particular, it cannot make any assumption on how many times names may be repeating. The only thing that is guaranteed is that these lists will be of equal lengths
